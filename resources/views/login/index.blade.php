@extends('layouts.main')

@section('content')
    <div class="login-box">
        <div class="card">
            <div class="card-body login-card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form id="login-form" action="{{ route('login-user') }}" method="POST">
            @csrf

            <div class="mb-3">
                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="E-mail" />
            </div>

            <div class="mb-3">
                <input type="password" name="password" id="password" class="form-control" placeholder="Hasło" />
            </div>

            <button class="btn btn-primary btn-block">Zaloguj</button>
        </form>
            </div>
        </div>
    </div>
@endsection