@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-12">
                        <h1>Wymiana walut</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ route('currencyExchange') }}" role="form" class="col-6 offset-3">
                @csrf
                <div class="form-group">
                    <input name="date" type="date"@if(!empty($minDate)) min="{{$minDate}}"@endif @if(!empty($maxDate)) max="{{$maxDate}}"@if(!empty($date)) value="{{$date}}"@else value="{{$maxDate}}"@endif @endif class="form-control">
                </div>

                @if(!empty($currencyList))
                    <div class="form-group">
                        <input type="number" name="amountToExchange"@if(!empty($formData['amountToExchange'])) value="{{$formData['amountToExchange']}}"@endif step=0.01 class="form-control">
                    </div>
                    <div class="form-group">
                        <select name="currencyToExchange" class="form-control">
                            @foreach($currencyList as $currency)
                                <option value="{{$currency->id}}"@if(!empty($formData['currencyToExchange']) && $formData['currencyToExchange'] == $currency->id) selected="selected"@endif>{{$currency->short_name}} ({{$currency->name}})</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if(!empty($currencyList))
                    <div class="form-group">
                        <input type="text" name="amountToReceive"@if(!empty($rateToReceive)) value="{{$rateToReceive}}"@endif readonly class="form-control">
                    </div>
                    <div class="form-group">
                        <select name="currencyToReceive" class="form-control">
                            @foreach($currencyList as $currency)
                                <option value="{{$currency->id}}"@if(!empty($formData['currencyToReceive']) && $formData['currencyToReceive'] == $currency->id) selected="selected"@endif>{{$currency->short_name}} ({{$currency->name}})</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <button class="btn btn-primary">Przelicz</button>
            </form>
        </div>
    </div>

@endsection