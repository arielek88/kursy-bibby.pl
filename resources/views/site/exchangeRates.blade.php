@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-12">
                        <h1>Wymiana walut</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <form method="post" action="{{ route('exchangeRates') }}" class="col-6 offset-3 mb-2">
                @csrf
                <div class="input-group">
                    <input name="date" type="date"@if(!empty($minDate)) min="{{$minDate}}"@endif @if(!empty($maxDate)) max="{{$maxDate}}"@if(!empty($date)) value="{{$date}}"@else value="{{$maxDate}}"@endif @endif class="form-control">
                    <span class="input-group-append">
                        <button class="btn btn-primary">Pokaż</button>
                    </span>
                </div>
            </form>

            @if(!empty($exchangeRatesList))
                <div class="card">
                    <table class="table table-hover text-nowrap">
                        <tr>
                            <th>Skrócona nazwa waluty</th>
                            <th>Nazwa waluty</th>
                            <th>Kurs</th>
                        </tr>
                        @foreach ($exchangeRatesList as $rate)
                            <tr>
                                <td>{{$rate->short_name}}</td>
                                <td>{{$rate->name}}</td>
                                <td>{{$rate->currencyRate[0]->rate}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection