<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link{{ (request()->is('/')) ? ' active' : '' }}">
                        <p>Strona główna</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('exchangeRates') }}" class="nav-link{{ (request()->is('kursy-walut')) ? ' active' : '' }}">
                        <p>Kursy walut</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('currencyExchange') }}" class="nav-link{{ (request()->is('wymiana-walut')) ? ' active' : '' }}">
                        <p>Wymiana walut</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link{{ (request()->is('wyloguj')) ? ' active' : '' }}">
                        <p>Wyloguj</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>