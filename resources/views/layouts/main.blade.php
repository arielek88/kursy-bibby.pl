<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte.min.css') }}">
    </head>
    <body @if(!empty($bodyClass)) class="{{$bodyClass}}"@endif>
        @auth
            @include('site.menu')
        @endauth
        @yield('content')

    </body>
</html>