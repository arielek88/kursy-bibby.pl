<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestExchangeForm extends TestCase
{
    use WithFaker;

    /**
     * Test exchange form.
     *
     */
    public function testFrom()
    {
        $this->withoutMiddleware();

        $response = $this->post(route('currencyExchange'), [
            'date' => $this->faker->date(),
            'amountToExchange' => $this->faker->randomNumber(4),
            'currencyToExchange' => $this->faker->randomNumber(2),
            'currencyToReceive' => $this->faker->randomNumber(2),
        ]);

        $response->assertStatus(200);
    }
}
