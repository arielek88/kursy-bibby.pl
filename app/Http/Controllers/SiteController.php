<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\ExchangeRates;
use Illuminate\Http\Request;
use Validator;

class SiteController extends Controller
{
    /**
     * Main page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        return view('site.index', ['bodyClass' => 'sidebar-mini layout-fixed']);
    }

    /** Exchange rates page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function exchangeRates(Request $request)
    {
        $date = $request->get('date');
        $exchangeRates = new ExchangeRates();
        $exchangeRatesList = [];
        if (!empty($date)) {
            $exchangeRatesList = Currency::with(['currencyRate' => function ($query) use ($date){
                $query->where('date', $date);
            }])->get();
        }

        $exchangeDates = $exchangeRates->distinct('date')->orderBy('date', 'asc')->pluck('date');
        $minDate = 0;
        $maxDate = 0;
        if (!empty($exchangeDates[0])) {
            $minDate = $exchangeDates[0];
        }
        if (!empty($exchangeDates[count($exchangeDates) - 1])) {
            $maxDate = $exchangeDates[count($exchangeDates) - 1];
        }

        return view('site.exchangeRates', [
            'minDate' => $minDate,
            'maxDate' => $maxDate,
            'exchangeRatesList' => $exchangeRatesList,
            'date' => $date,
            'bodyClass' => 'sidebar-mini layout-fixed'
        ]);
    }

    /**
     * Currency exchange page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function currencyExchange(Request $request)
    {
        //
        $formData = $request->all();
        $errors = [];
        $exchangeRates = new ExchangeRates();
        if (!empty($formData)) {
            // Walidacja.
            $validator = Validator::make($formData, [
                'date' => 'required',
                'amountToExchange' => 'required|numeric',
                'currencyToExchange' => 'required|exists:currency,id|different:currencyToReceive',
                'currencyToReceive' => 'required|exists:currency,id',
            ]);
            $errors = $validator->errors();

            if (empty($errors->any())) {
                $rateToExchange = $exchangeRates->where([
                    ['currency_id', '=', $formData['currencyToExchange']],
                    ['date', '=', $formData['date']]])
                    ->first();

                $rateToReceive = $exchangeRates->where([
                    ['currency_id', '=', $formData['currencyToReceive']],
                    ['date', '=', $formData['date']]])
                    ->first();

                if (!empty($rateToReceive->rate)) {
                    $rateToExchangePln = $formData['amountToExchange'] * $rateToExchange->rate;
                    $rateToReceiveFromPln = $rateToExchangePln / $rateToReceive->rate;
                }
            }
        }

        $exchangeDates = $exchangeRates->distinct('date')->orderBy('date', 'asc')->pluck('date');
        $minDate = 0;
        $maxDate = 0;
        if (!empty($exchangeDates[0])) {
            $minDate = $exchangeDates[0];
        }
        if (!empty($exchangeDates[count($exchangeDates) - 1])) {
            $maxDate = $exchangeDates[count($exchangeDates) - 1];
        }

        $currencyList = Currency::all();

        return view('site.currencyExchange', [
            'minDate' => $minDate,
            'maxDate' => $maxDate,
            'currencyList' => $currencyList,
            'errors' => $errors,
            'rateToReceive' => (!empty($rateToReceiveFromPln)
                ? number_format($rateToReceiveFromPln, 2, ',', ' ') : ''),
            'formData' => (!empty($formData) ? $formData : ''),
            'bodyClass' => 'sidebar-mini layout-fixed'
        ]);
    }
}
