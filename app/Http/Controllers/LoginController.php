<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class LoginController extends Controller
{
    /**
     * Login site.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        return view('login.index', ['bodyClass' => 'login-page']);
    }

    /**
     * Login action.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $userData = $request->only('email', 'password');
        $validator = Validator::make(
            $userData,
            [
                'email'    => 'required|email',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->route('login')->withErrors($validator)->withInput();
        }

        if (Auth::attempt($userData)) {
            return redirect()->home();
        } else {
            return redirect()->route('login')
                ->withErrors(['msg' => 'Logowanie nie powiodło się'])
                ->withInput();
        }
    }

    /**
     * Logout action.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
