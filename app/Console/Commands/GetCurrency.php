<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getCurrency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('NBP_API_URL') . 'exchangerates/tables/a/?format=json';

        $currenciesList = Http::get($url)->json();

        if (!empty($currenciesList[0]['rates'])) {
            foreach ($currenciesList[0]['rates'] as $singleCurrency) {
                Currency::insertOrIgnore([
                    [
                        'short_name' => $singleCurrency['code'],
                        'name' => $singleCurrency['currency']
                    ]
                ]);
            }
        }

        return 0;
    }
}
