<?php

namespace App\Console\Commands;

use App\Models\Currency;
use App\Models\ExchangeRates;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getExchangeRates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currencyList = Currency::all();

        if (!empty($currencyList)) {
            $dateStart = date('Y-m-d', strtotime('-10 days midnight'));
            $dateEnd = date('Y-m-d');
            foreach ($currencyList as $currency) {
                $url = env('NBP_API_URL') . 'exchangerates/rates/a/'
                    . $currency->short_name . '/'
                    . $dateStart . '/'
                    . $dateEnd . '/'
                    . '?format=json';
                $exchangeRatesResponse = Http::get($url)->json();

                if (!empty($exchangeRatesResponse['rates'])) {
                    $tempTable = [];
                    foreach ($exchangeRatesResponse['rates'] as $rate) {
                        $tempTable[$rate['effectiveDate']] = $rate['mid'];
                    }

                    for ($i = strtotime($dateStart); $i <= strtotime($dateEnd); $i = strtotime(date('Y-m-d', $i) . ' +1 day')) {
                        if (!empty($tempTable[date('Y-m-d', $i)])) {
                            $rateValue = $tempTable[date('Y-m-d', $i)];
                        }

                        if (!empty($rateValue)) {
                            ExchangeRates::insertOrIgnore([
                                [
                                    'currency_id' => $currency->id,
                                    'rate' => $rateValue,
                                    'date' => date('Y-m-d', $i)
                                ]
                            ]);
                        }
                    }
                }
            }
        }


        return 0;
    }
}
