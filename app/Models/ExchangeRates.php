<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExchangeRates extends Model
{
    use HasFactory;

    /**
     * Timestamps columns.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * ExchangeRates construct.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rates()
    {
        return $this->belongsTo('App\Models\Currency', 'id', 'currency_id');
    }
}
