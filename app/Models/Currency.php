<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'currency';

    /**
     * Timestamps columns
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Currency construct.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function currencyRate()
    {
        return $this->hasMany('App\Models\ExchangeRates', 'currency_id', 'id');
    }
}
