# README #

How to run this application

Get Repository from bitbucket url
[here](https://bitbucket.org/arielek88/kursy-bibby.pl/src/master/)

## With Docker ##
After cloning project open console or in windows cmd and go to project directory and run
```
docker-compose up -d --build
```

## Copy .env file ##
```
cp .env.example .env
```

## If you don`t use docker ##
You must edit .env file and supplement this variables
```
APP_URL=http://localhost:8081

DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=bibby
DB_USERNAME=bibby
DB_PASSWORD=secret
``` 
## Install composer ##
```
composer install
```
if you don't use docker and haven't installed composer you must run this command
```
curl -sS https://getcomposer.org/installer | \
php -- --install-dir=/usr/bin/ --filename=composer
```

## Generate app key ##
Run this command
```
php artisan key:generate
```

## Migration ##
Run this commands
```
php artisan migrate
```
```
php artisan db:seed
```

## Run console commands ##
Get currencies list from NBP
```
php artisan command:getCurrency
```
Get currencies rates from NBP
```
php artisan command:getExchangeRates
```

## Login data ##
login: admin@admin.com
password: admin1234

## Test commands ##
```
./vendor/bin/phpunit ./tests/Feature/TestExchangeForm.php
```